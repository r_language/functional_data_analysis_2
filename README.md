# Functional_Data_Analysis_2

In this rmd file you can have a look at a functional data analysis more advanced than the first functional data analysis. 

We realized a functional data analysis thanks to gait dataset available in fda library. We can recap the different steps that we did :

1 - Load and Explore data

2 - Definition of the harmonic operator acceleration

3 - Create basis to produce the curve between the points/values 

4 - Choice for lambda, smooth parameters

5 - Produce curves and first derivatives

6 - Descriptive statistics (mean, variance, covariance of the functions)

6-1- Descriptive statistics - Mean

6-2- Descriptive statistics - Variance and covariance

6-3- Descriptive statistics - Correlation

7- Functional PCA

8 - Scores associated to each angle

9 - Mean values of our functions

10 - Difference between smoothed values and mean functions 

11- Mean variance of the residuals

12 - Calculate adjustment of residuals for functional PCA

13 - Calculate adjusted variances of the functional PCA 

14 - Calculate relative percentages to the adjusted variances of the functional PCA 

15 - Calculate the relative percentages of the mean adjusted variances

16 - Analysis of the canonical correlation

17 - Save angular acceleration

17-1- Calculate acceleration and mean acceleration

17-2- Spline basis for the deformation functions of the angular acceleration

17-3 - Prediction of the knee angle based on the hip angle

18 - Regression of functional data 

19 - Calculate the squared correlation function (multiple)

20 - Prediction of the knee acceleration based on the hip acceleration

21- Calculate and represent the regression functions

22 - Calculate and represent the squared correlation function (multiple)


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

December 2022
